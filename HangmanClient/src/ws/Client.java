package ws;

import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.axis2.AxisFault;

import ws.ServiceStub.AllGames;
import ws.ServiceStub.AllGamesResponse;
import ws.ServiceStub.GameStatus;
import ws.ServiceStub.GameStatusResponse;
import ws.ServiceStub.Guess;
import ws.ServiceStub.NewGame;

public class Client {

	/**
	 * Minimalist client for the Web Service Hangman
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int option = -1;
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		ServiceStub st = null;
		try {
			st = new ServiceStub();
		} catch (AxisFault e1) {
			e1.printStackTrace();
		}
		while (option != 0) {
			System.out.println("\t[1] Start new game\n\t[2] View all games"
					+ "\n\t[3]Check game status\n\t[4]Guess"
					+ "\n\t[5]Credits\n\t[0]Exit");
			System.out.println("\n\t>>");
			option = keyboard.nextInt();
			// OPTION 1: Start a new game
			if (option == 1) {
				newGame(st);
			}
			// OPTION 2: View all the games in progress (list of IDs)
			else if (option == 2) {
				allGames(st);
			}
			// OPTION 3: View game status usign gameID
			else if (option == 3) {
				System.out.println("\t>> >> Enter GameID");
				short gid = keyboard.nextShort();
				checkGameStatus(st, gid);
			}
			// OPTION 4: Guess a char (gameID and char)
			else if (option == 4) {
				System.out.println("\t>> >> Enter GameID");
				short gid = keyboard.nextShort();
				System.out.println("\t>> >> Enter letter [a-z]");
				String userchar = keyboard.next();
				guessCharGame(st, gid, userchar);
				checkGameStatus(st, gid);
			}
			// OPTION 5: Credits
			else if (option == 5) {
				System.out.println("\n======[CREDITS]======\n");
				System.out
						.println("project:\tHangman by Raul Reguillo Carmona");
				System.out.println("e-mail:\t\traul.reguillo@gmail.com");
				System.out.println("\n=======================\n");
			}
			// OPTION 0: Exit
			else if (option == 0) {
				System.out.println("Bye! Have a nice day!");
			}
			// In other cases
			else {
				System.out.println("Not an option. Please try again [0-5]");
			}
		}

	}

	private static void newGame(ServiceStub st) {
		try {
			NewGame ng = new NewGame();
			st.newGame(ng);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * allGames method Encapuslates the call we need
	 * 
	 * @param st
	 */
	private static void allGames(ServiceStub st) {
		AllGames ag = new AllGames();
		try {
			AllGamesResponse agr = st.allGames(ag);
			System.out.println(agr.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Guess letter method Encapsulates the call we need
	 * 
	 * @param st
	 * @param gid
	 * @param userchar
	 */
	private static void guessCharGame(ServiceStub st, short gid, String userchar) {
		Guess gword = new Guess();
		Pattern p = Pattern.compile("[a-z]");
		if (p.matcher(userchar.charAt(0) + "").find()) {
			gword.setGameId(gid);
			gword.setG(userchar.charAt(0) + "");
			try {
				st.guess(gword);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Not a valid char (valids are [a-z])");
		}

	}

	/**
	 * Check a particular game's status Encapsulates the call we need
	 * 
	 * @param st
	 * @param gid
	 */
	public static void checkGameStatus(ServiceStub st, short gid) {
		GameStatus gs = new GameStatus();
		gs.setGameId(gid);
		try {
			GameStatusResponse gsr = st.gameStatus(gs);
			System.out.println(gsr.get_return());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
