package ws;

import game.Game;
import game.GameListSingleton;

/**
 * 
 * Web Service Hangman with 4 operations:
 * 
 * - newGame(): Creates a new Game. Does not return any data. - allGames():
 * Returns the list of all games created - guess(gameID, guessed char): Guess
 * the given char into the given game. Does not return any data. -
 * gameStatus(gameID): Returns the JSON status of the given game.
 * 
 * @author Ra�l Reguillo Carmona
 * 
 */
public class Service {

	public static void newGame() {

		if (GameListSingleton.getInstance().getGameList().size() == 0)
			GameListSingleton.getInstance().getGameList()
					.add(new Game((short) 0));
		else
			GameListSingleton
					.getInstance()
					.getGameList()
					.add(new Game((short) GameListSingleton.getInstance()
							.getGameList().size()));

	}

	public static String allGames() {
		return GameListSingleton.getInstance().getGameList().toString();
	}

	public static void guess(short gameId, char g) {
		GameListSingleton.getInstance().getGameList().get(gameId).guessWord(g);
	}

	public static String gameStatus(short gameId) {
		String data = GameListSingleton.getInstance().getGameList().get(gameId)
				.getGameState();
		return data;
	}
}
