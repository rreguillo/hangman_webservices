package game;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.catalina.connector.Request;

/**
 * Class of the Hangman Game.
 * 
 * - gameId: The ID of the game
 * - triesLeft: how many tries left to lose the game
 * - gameWord: randomly picked word
 * - userWord: the word which will be sent to the user when he/she ask for game states
 * - gameStatus: busy || fail || success
 * 
 * @author Ra�l Reguillo Carmona
 * 
 */
public class Game {
	private short gameId;
	private short triesLeft;
	private String gameWord;
	private String userWord;
	private String gameStatus;
	// TODO change 
	static final String FILEPATH = "D:\\files\\words.english";

	/** Setters & Getters **/
	public short getGameId() {
		return gameId;
	}

	public short getTriesLeft() {
		return triesLeft;
	}

	public String getGameWord() {
		return gameWord;
	}

	public String getUserWord() {
		return userWord;
	}

	public String getGameStatus() {
		return gameStatus;
	}

	public void setGameId(short gameId) {
		this.gameId = gameId;
	}

	public void setTriesLeft(short triesLeft) {
		this.triesLeft = triesLeft;
	}

	public void setGameWord(String gameWord) {
		this.gameWord = gameWord;
	}

	public void setUserWord(String userWord) {
		this.userWord = userWord;
	}

	public void setGameStatus(String gameStatus) {
		this.gameStatus = gameStatus;
	}

	/**
	 * Constructor: using a new Game ID Everything else is auto-generated
	 * 
	 * @param gameId
	 */
	public Game(short gameId) {
		setGameId(gameId);
		setTriesLeft((short) 11);
		setGameStatus("busy");
		getRandomWord();
	}

	/**
	 * Empty constructor
	 */
	public Game() {
		super();
	}

	/**
	 * Pick a random word from the given list which is setted to the parameter.
	 * 
	 */
	private void getRandomWord() {

		// Pick random word
		Random rand = new Random(System.currentTimeMillis());
		try {

			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new FileReader(FILEPATH));
			String line = reader.readLine();
			List<String> words = new ArrayList<String>();
			while (line != null) {
				String[] wordsLine = line.split("\n");
				for (String word : wordsLine) {
					words.add(word);
				}
				line = reader.readLine();
			}

			// Setting game word
			setGameWord(words.get(rand.nextInt(words.size())));
		} catch (Exception e) {
			setGameWord("****");
			e.printStackTrace();
		}

		// Setting user word
		String uword = "";
		for (int i = 0; i < getGameWord().length(); i++)
			uword += "�";
		setUserWord(uword);
		System.out.println(getGameWord() + "\n" + getUserWord() + "\n");
	}

	/**
	 * Check if the letter is contained in the word
	 * 
	 * @return
	 */
	public String guessWord(char g) {

		/*
		 * if (status == busy && gameWord contains g)
		 *   update userWord 
		 *   if userWord is finished 
		 *      change game status 
		 *      else 
		 *      triesLeft --
		 *         if (triesLeft == 0) 
		 *            change status fail
		 */
		if (getGameStatus().equals("busy")) {
			if (getGameWord().contains(g + "")
					&& !getUserWord().contains(g + "")) {
				String newWord = getUserWord();
				char[] nwarray = newWord.toCharArray();
				for (int i = 0; i < getGameWord().length(); i++)
					if (getGameWord().charAt(i) == g) {
						nwarray[i] = g;
					}
				setUserWord(String.valueOf(nwarray));
				if (!getUserWord().contains("�"))
					setGameStatus("success");
			} else {
				setTriesLeft((short) (getTriesLeft() - 1));
				if (getTriesLeft() == 0)
					setGameStatus("fail");
			}
		}
		return getUserWord();
	}

	/**
	 * Returns the game status in JSON
	 * 
	 * @return JSON game status
	 */
	public String getGameState() {
		// Not so complicated JSON string, so is builded in this way
		return "{'GameID':" + getGameId() + ", 'Word':'" + getUserWord()
				+ "', 'TriesLeft':" + getTriesLeft() + ", 'Status':'"
				+ getGameStatus() + "' }";
	}

	/**
	 * Also JSON parse could be here
	 */
	public String toString() {
		return "GameID: " + getGameId() + " - STATUS: " + getGameStatus();
	}

}
