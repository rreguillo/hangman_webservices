package game;

import java.util.ArrayList;

/**
 * Using a Singleton class to keep all the games in progress. Fast and easier
 * solution.
 * 
 * @author Ra�l Reguillo Carmona
 * 
 */
public final class GameListSingleton {
	private static volatile GameListSingleton instance = null;
	private ArrayList<Game> gameList;

	private GameListSingleton() {
		gameList = new ArrayList<Game>();
	}

	public ArrayList<Game> getGameList() {
		return gameList;
	}

	public static GameListSingleton getInstance() {
		if (instance == null) {
			synchronized (GameListSingleton.class) {
				instance = new GameListSingleton();
			}
		}
		return instance;
	}
}
